const domains = require('./domains.json')

module.exports = phrase =>
  domains
    .filter(domain => phrase.indexOf(domain.name) > 0)
    .map(domain => {
      const splitted = phrase.replace(/\s/g, '.').split(domain.name)
      while (splitted.length > 2) {
        splitted[0] = `${splitted.shift()}${domain.name}${splitted[0]}`
      }
      let result
      if (splitted[1] === '') {
        result = `${splitted[0]}.${domain.name}`
      } else {
        result = `${splitted[0]}.${domain.name}/${splitted[1]}`
      }
      return {
        str: result.replace(/\.{2}/g, '.').replace(/\/\./g, '/'),
        entity: domain.entity
      }
    })
