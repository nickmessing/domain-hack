require('./check-versions')()

process.env.NODE_ENV = 'production'

const fs = require('fs')
const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('./webpack.prod.conf')

const spinner = ora('building for production...')

const expressCode = 
`const express = require('express')
const domainHacks = require('./domainHacks')
const app = express()
app.use('/', express.static('./front-end'))
app.get('/hacks', (req, res) => {
  res.json(domainHacks(req.query.phrase))
})
app.listen(process.env.PORT || 8080)
`

const remove = () => new Promise((resolve, reject) => {
  rm(path.join(config.build.serverRoot, config.build.assetsSubDirectory), err => {
    if (err) {
      reject(err)
    } else {
      resolve()
    }
  })
})

const bundle = () => new Promise((resolve, reject) => {
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      reject(err)
    } else {
      resolve(stats)
    }
  })
})

const saveFile = (name, source) => new Promise((resolve, reject) => {
  fs.writeFile(name, source, err => {
    if (err) {
      reject(err)
    } else {
      resolve()
    }
  })
})

const readFile = name => new Promise((resolve, reject) => {
  fs.readFile(name, 'utf-8', (err, data) => {
    if (err) {
      reject(err)
    } else {
      resolve(data)
    }
  })
})

const copyFile = async (to, from) => {
  await saveFile(to, await readFile(from))
}

const generateServer = () => Promise.all([
  saveFile(path.join(config.build.serverRoot, 'server.js'), expressCode),
  copyFile(path.join(config.build.serverRoot, 'domainHacks.js'), path.join(__dirname, '..', 'src', 'domainHacks.js')),
  copyFile(path.join(config.build.serverRoot, 'domains.json'), path.join(__dirname, '..', 'src', 'domains.json'))
])

const build = async () => {
  try {
    spinner.start()
    await remove()
    const stats = await bundle()
    await generateServer()

    spinner.stop()

    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: to run do "cd dist && node server"'
    ))
  } catch (e) {
    spinner.stop()
    console.error(e)
  } 
}

build()

// rm(path.join(config.build.serverRoot, config.build.assetsSubDirectory), err => {
//   if (err) throw err
//   webpack(webpackConfig, function (err, stats) {
//     spinner.stop()
//     if (err) throw err
//     fs.writeFileSync(path.join(config.build.serverRoot, 'server.js'), expressCode)
//     fs.writeFileSync(path.join(config.build.serverRoot, 'domainHacks.js'), fs.readFileSync(path.join(__dirname, '..', 'src', 'domainHacks.js')))
//     fs.writeFileSync(path.join(config.build.serverRoot, 'domains.jons'), fs.readFileSync(path.join(__dirname, '..', 'src', 'domainHacks.js')))
//     process.stdout.write(stats.toString({
//       colors: true,
//       modules: false,
//       children: false,
//       chunks: false,
//       chunkModules: false
//     }) + '\n\n')

//     console.log(chalk.cyan('  Build complete.\n'))
//     console.log(chalk.yellow(
//       '  Tip: built files are meant to be served over an HTTP server.\n' +
//       '  Opening index.html over file:// won\'t work.\n'
//     ))
//   })
// })
