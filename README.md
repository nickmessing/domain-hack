# titanium

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Domains

Domains were parsed from [wikipedia TLDs page](https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains) using this code:

```js
JSON.stringify(
  [...document.querySelectorAll('tbody>tr>td')]
    .map(el => el.children.length ? el.children[0].innerText : el.innerText)
    .filter(el => el.match(/^\.[a-z]{2,}$/))
    .map(el => el.substr(1))
    .reduce((arr, el) => {
      if (arr.includes(el)) {
        return arr
      } else {
        return [...arr, el]
      }
    }, [])
)
```

For detailed information with flags:

```js
JSON.stringify(
  [...document.querySelectorAll('tbody>tr')]
    .map(el => ({
      name: el.children[0].textContent,
      entityEl: el.children[1]
    }))
    .filter(el => el.name.match(/^\.[a-z]{2,}$/))
    .map(el => ({
      name: el.name.substr(1),
      entity: {
        image: el.entityEl.querySelector('img') && el.entityEl.querySelector('img').src,
        text: el.entityEl.textContent.trim()
      }
    }))
, null, '  ')
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
