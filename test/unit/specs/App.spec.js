import Vue from 'vue'
import App from '@/App'

const sleep = time => new Promise(resolve => setTimeout(resolve, time))

window.fetch = () => Promise.resolve({
  json: () => Promise.resolve([
    {
      str: 'domain',
      entity: {
        text: 'mock'
      }
    }
  ])
})

describe('Hello.vue', () => {
  it('should render correct contents', async () => {
    const Constructor = Vue.extend(App)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelectorAll('ul > li').length).to.equal(0)
    vm.$el.querySelector('input').value = 'titanium software it'
    vm.$el.querySelector('input').dispatchEvent(new Event('input'))
    await Vue.nextTick()
    const list = vm.$el.querySelectorAll('ul > li')
    expect([...list].map(el => el.textContent.trim())).to.eql(['(Loading...)'])
    await sleep(600)
    const list2 = vm.$el.querySelectorAll('ul > li')
    expect([...list2].map(el => el.textContent.trim())).to.eql(['domain (mock)'])
  })
})
