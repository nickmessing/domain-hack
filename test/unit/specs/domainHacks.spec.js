import domainHacks from '@/domainHacks'

describe('domainHacks.js', () => {
  it('should generate correct domain hacks', () => {
    expect(domainHacks('titanium software it')).to.eql([
      {
        str: 'tit.an/ium.software.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_the_Netherlands_Antilles_%281986-2010%29.svg/23px-Flag_of_the_Netherlands_Antilles_%281986-2010%29.svg.png',
          text: 'Netherlands Antilles'
        }
      },
      {
        str: 'titanium.softw.ar/e.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Flag_of_Argentina.svg/23px-Flag_of_Argentina.svg.png',
          text: 'Argentina'
        }
      },
      {
        str: 'titanium.software.it',
        entity: {
          image: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/23px-Flag_of_Italy.svg.png',
          text: 'Italy'
        }
      },
      {
        str: 'tita.ni/um.software.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Nicaragua.svg/23px-Flag_of_Nicaragua.svg.png',
          text: 'Nicaragua'
        }
      },
      {
        str: 'titanium.softwa.re/it',
        entity: {
          image: 'https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/23px-Flag_of_France.svg.png',
          text: 'Réunion'
        }
      },
      {
        str: 'titanium.so/ftware.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Flag_of_Somalia.svg/23px-Flag_of_Somalia.svg.png',
          text: 'Somalia'
        }
      },
      {
        str: 'titanium.sof.tw/are.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Flag_of_the_Republic_of_China.svg/23px-Flag_of_the_Republic_of_China.svg.png',
          text: 'Taiwan'
        }
      },
      {
        str: 'titani.um/software.it',
        entity: {
          image:
            'https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/23px-Flag_of_the_United_States.svg.png',
          text: 'United States Minor Outlying Islands'
        }
      },
      { str: 'titanium.software/it', entity: { image: null, text: 'computer software' } }
    ])
  })
})
